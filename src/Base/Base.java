package Base;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import Hash.Hashing;
import GUI.GUI;


public class Base {
	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Base();
	}
	public Base() {
		frame = new GUI();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(600,600);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		
		
		Hashing Test = new Hashing();
		String a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12;
		a1="http://course.eau.ac.th/course/Download/0230805/PPT17.ppt";
		a2="http://phakwalan79.blogspot.com/2012/08/blog-post_8410.html?m=1";
		a3="http://blog.bossturteam.com/sql";
		a4="https://anttawiporn.wordpress.com/2012/07/17/7-2";
		a5="http://itd.htc.ac.th/st_it50/it5027/image/xml/Ex7.html";
		a6="http://www.computer.cmru.ac.th/pornwana/web/wp-content/uploads/2009/04/chapter1.ppt";
		a7="http://kampol.htc.ac.th/web1/subject/assembly/sheet/iew/chapter1.htm";
		a8="http://staff.cs.psu.ac.th/iew/cs344-321/chapter1.doc";
		a9="http://km.cpc.ac.th/wee2eew/files/2011/12/";
		a10="http://m.pantip.com/topic/31827933";
		a11="http://m.pantip.com/topic/32163594";
		a12="http://www.jeban.com/viewtopic.php?t=196703";
		
		frame.setResult	(a1+ "\n"+"your hash is "+Test.mod(Test.ascii(a1))+"\n"
						+(a2+ "\n"+"your hash is "+Test.mod(Test.ascii(a2)))+"\n"
						+(a3+ "\n"+"your hash is "+Test.mod(Test.ascii(a3)))+"\n"
						+(a4+ "\n"+"your hash is "+Test.mod(Test.ascii(a4)))+"\n"
						+(a5+ "\n"+"your hash is "+Test.mod(Test.ascii(a5)))+"\n"
						+(a6+ "\n"+"your hash is "+Test.mod(Test.ascii(a6)))+"\n"
						+(a7+ "\n"+"your hash is "+Test.mod(Test.ascii(a7)))+"\n"
						+(a8+ "\n"+"your hash is "+Test.mod(Test.ascii(a8)))+"\n"
						+(a9+ "\n"+"your hash is "+Test.mod(Test.ascii(a9)))+"\n"
						+(a10+ "\n"+"your hash is "+Test.mod(Test.ascii(a10)))+"\n"
						+(a11+ "\n"+"your hash is "+Test.mod(Test.ascii(a11)))+"\n"
						+(a12+ "\n"+"your hash is "+Test.mod(Test.ascii(a12))));
		
		

	}
	ActionListener list;
	GUI frame;
}

